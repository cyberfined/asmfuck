# asmfuck
Brainfuck interpreter, written in nasm for OS Linux 32 bit.
# Dependencies
nasm
# Building
```bash
git clone https://Undefined3102@bitbucket.org/Undefined3102/asmfuck.git
cd asmfuck
make
```
# Usage
asmfuck <filename>
# License
GPLv3
# Author
Undefined3102@gmail.com
